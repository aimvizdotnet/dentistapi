const Sequelize = require('sequelize');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var config = require('./config'); 
const cors = require('cors');

// enable CORS for cross domain AJAX calls
const corsOptions = {
  origin: config.corsURL
}

module.exports = app;
// app configuration
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('superSecret', config.secret);
app.use(cors(corsOptions));
app.use(express.static('web'));
// initialize database
sequelize = new Sequelize(config.database, config.db_user, config.db_pass, {
    host: config.db_server,
    dialect: config.db_type,

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

// initialize routes (API end-points)
require('./app/routes/routes.js')(app, {});

// start http server
app.listen(config.port, () => { 
    console.log('Server running on port : ' + config.port);
});






