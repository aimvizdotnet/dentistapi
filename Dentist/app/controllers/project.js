let Project = require('../models/project');
var app = require('../../server.js');
var jwt = require('jsonwebtoken');

function GetAllProjects(req, res) {
    Project.sync({}).then(function () {
        // Table created
        return Project.findAll()
            .then(function (content) {
                res.send(content);
            });
    }).catch(function (error) {
        console.log('Unable to connect to the database: ', error);
    });
}

function GetProjectByID(req, res) {
    const id = req.params.id;
    return Project.findOne({
        where: {
            Project_Code: id
        }
    }).then(function (content) {
        res.send(content);
    });

}

//export all the functions
module.exports = { GetAllProjects, GetProjectByID };