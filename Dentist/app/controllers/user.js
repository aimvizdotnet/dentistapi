let User = require('../models/user');
let Customer = require('../models/Customer');
var app = require('../../server.js');
var jwt = require('jsonwebtoken');

//function Signup(req, res) {
//    Customer.sync({}).then(function () {

//        var email = req.body.email;
//        return User.findOne({
//            where: {
//                Email: email
//            }
//        }).then(function (content) {

//            if (content) {
//                res.json({ success: false, message: "Email address already exists in our system." });
//            } else {

//                var fullName = req.body.name.split(' ');
//                var firstName = fullName[0];
//                var lastName = fullName.length > 1 ? fullName[1] : '';
//                var password = req.body.password;
//                User.create({
//                    First_Name: firstName,
//                    Last_Name: lastName,
//                    Email_Address: email,
//                    Password: req.body.password
//                }).then(function (result) {
//                    user = result.dataValues;
//                    // create a token
//                    var token = jwt.sign(user, app.get('superSecret'), {
//                        expiresIn: 60 * 60 * 24 // expires in 24 hours
//                    });

//                    // return the information including token as JSON
//                    res.json({
//                        success: true,
//                        message: 'Enjoy your token!',
//                        token: token,
//                        id: user.User_Code,
//                        firstName: firstName,
//                        lastName: lastName
//                    });
//                });
//            }

//        });
//    }).catch(function (error) {
//        console.log('Unable to connect to the database: ', error);
//    });
//}

function Login(req, res) {

    Login.sync({}).then(function () {

        var email = req.body.email;
        return Login.findOne({
            where: {
                Email: email
            }
        }).then(function (content) {

            if (!content) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            } else {

                var user = content.dataValues;
                // check if password matches
                if (req.body.password != user.Password) {
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                } else {

                    // if user is found and password is right
                    // create a token
                    var token = jwt.sign(user, app.get('superSecret'), {
                        expiresIn: 60 * 60 * 24 // expires in 24 hours
                    });

                    // return the information including token as JSON
                    res.json({
                        success: true,
                        message: 'Logged in successfully!',
                        token: token,
                        id: user.ID_Login_Endkunde,
                        Email: user.Email
                    });

                }
            }
        });
    }).catch(function (error) {
        console.log('Unable to connect to the database: ', error);
    });
}


//function GetAllUsers(req, res) {
//    User.sync({}).then(function () {
//        return User.findAll()
//            .then(function (content) {
//                res.send(content);
//            });
//    }).catch(function (error) {
//        console.log('Unable to connect to the database: ', error);
//    });
//}

//export all the functions
module.exports = { Login };