﻿const Sequelize = require('sequelize');
var app = require('../../server.js');
var jwt = require('jsonwebtoken');
let Service = require('../models/Services');
let ServiceType = require('../models/ServiceType');
let Practitioner = require('../models/Practitioner');
let PractitionerService = require('../models/PractitionerService');
let Customer = require('../models/Customer');
let Appointment = require('../models/Appointment');

Service.belongsTo(ServiceType, { foreignKey: 'ServiceTypeId' });
Practitioner.belongsTo(Service, { foreignKey: 'ServiceId' });
PractitionerService.belongsTo(Service, { foreignKey: 'ID_Service' });
PractitionerService.belongsTo(Practitioner, { foreignKey: 'ID_DLE_rbringer' });
//var services = [
//    {
//        id: 1,
//        description: "Initial examination & checkup incl.2 xrays",
//        type: "CHF-128",
//        duration: "30mins"
//    },
//    {
//        id: 2,
//        description: "checkup incl.2 xrays",
//        type: "CHF-129",
//        duration: "40mins"
//    },
//    {
//        id: 3,
//        description: "Initial examination",
//        type: "CHF-130",
//        duration: "20mins"
//    },
//    {
//        id: 4,
//        description: "Initial examination & checkup incl.2 xrays",
//        type: "CHF-131",
//        duration: "50mins"
//    },
//    {
//        id: 5,
//        description: "Initial examination & checkup incl.4 xrays",
//        type: "CHF-132",
//        duration: "80mins"
//    },
//    {
//        id: 6,
//        description: "Initial examination & checkup incl.3 xrays",
//        type: "CHF-133",
//        duration: "60mins"
//    },
//    {
//        id: 7,
//        description: "checkup incl.1 xrays",
//        type: "CHF-134",
//        duration: "40mins"
//    },
//    {
//        id: 8,
//        description: "Initial examination & checkup incl.1 xrays",
//        type: "CHF-135",
//        duration: "30mins"
//    }];


function GetAllPractitioners(req, res) {
    var practitioners = [];

    Practitioner.findAll({
        where: {
            PracticeId: req.body.practiceId,
        }
    }).then(function (content) {
        practitioners = content;

        var values = {

            IsSuccess: true,
            Services: services,
            Practitioners: practitioners
        }

        return res.send(values);

    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Services: null,
            Practitioners: null,
            Message: "Something went wrong, please try again later!"
        }

        return res.send(values);
    });
}


function GetPractitionersByServiceID(req, res) {
    var practitioners = [];
    var service = {};
    PractitionerService.findAll({
        include: [
            { model: Practitioner },
            { model: Service }
        ],
        //, PracticeId: req.body.practiceId 
        where: { ID_Service: req.body.serviceId }
    }).then(function (content) {

        //practitioners = content;
        if (content != null) {

            for (var i = 0; i < content.length; i++) {
                practitioners.push(content[i].Practitioner)
                // console.log(element);
            }
        }
        var values = {

            IsSuccess: true,
            Service: service,
            Practitioners: practitioners
        }

        return res.send(values);


    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Practitioners: null,
            Service: null,
            Message: "Something went wrong, please try again later!"
        }

        return res.send(values);
    });

}
function getPractionerAllApointment(req, res) {
    var practitioners = [];
    var service = {};
    Practitioner.findOne({

        where: { ID_DLE_rbringer: 1 }
    }).then(function (content) {

        all = content.dataValues;

        var values = {


            all: all
        }

        return res.send(values);

    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Practitioners: null,
            Service: null,
            Message: "Something went wrong, please try again later!"
        }

        return res.send(values);
    });

}
function GetPractitionersOfService(req, res) {
    var practitioners = [];
    var service = {};
    Practitioner.findAll({
        include: [
            { model: Service }
        ],
        where: { ServiceId: req.body.serviceId, PracticeId: req.body.practiceId }
    }).then(function (content) {

        practitioners = content;

        Service.findOne(
            {
                where: { ID_Service: req.body.serviceId },
                include: [
                    { model: ServiceType }
                ]

            }
        ).then(function (content) {
            service = content;

            var values = {

                IsSuccess: true,
                Service: service,
                Practitioners: practitioners
            }

            return res.send(values);

        }
            ).catch(function (error) {

                console.log('Unable to connect to the database: ', error);

                var values = {

                    IsSuccess: false,
                    Service: null,
                    Practitioners: null,
                    Message: "Something went wrong, please try again later!"
                }
                return res.send(values);
            });

    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Practitioners: null,
            Service: null,
            Message: "Something went wrong, please try again later!"
        }

        return res.send(values);
    });

}
function insertBookingInformation(req, res) {
    //var practitioners = [];
    //var service = {};
    var email = req.body.Email;
    if (req.body.ID_Endkunde == null) {
        return Customer.findOne({
            where: {
                Email: email
            }
        }).then(function (content) {

            if (content) {
                res.json({ success: false, message: "Email address already exists in our system." });
            } else {

                var name = req.body.firstName + " " + req.body.lastName;
                var addressCity = req.body.addressCity;

                var addressPostalCode = req.body.addressPostalCode;
                var addressStreet = req.body.addressStreet;
                var addressStreetNumber = req.body.addressStreetNumber;
                var Phone = req.body.Phone;
                Customer.create({
                    Name: name,
                    Email: email,
                    addressCity: req.body.addressCity,
                    addressPostalCode: req.body.addressPostalCode,
                    addressStreet: req.body.addressStreet,
                    addressStreetNumber: req.body.addressStreetNumber,
                    Phone: req.body.Phone,
                    Password: "123456"
                }).then(function (result) {
                    var values = {

                        IsSuccess: true,

                        Message: "Your Booking has been made!"
                    }
                    return res.send(values);


                }).catch(function (error) {

                    console.log('Unable to connect to the database: ', error);

                    var values = {

                        IsSuccess: false,
                        Services: null,
                        Practitioners: null,
                        Message: "Something went wrong, please try again later!"
                    }
                    return res.send(values);
                });
            }

        }).catch(function (error) {

            console.log('Unable to connect to the database: ', error);

            var values = {

                IsSuccess: false,
                Services: null,
                Practitioners: null,
                Message: "Something went wrong, please try again later!"
            }
            return res.send(values);
        });
    }
    else {
        var values = {

            IsSuccess: true,

            Message: "Your Booking has been made!"
        }
        return res.send(values);
    }


}
function login(req, res) {
    //var practitioners = [];
    //var service = {};
    var email = req.body.Email;
    var Password = req.body.Password;
    return Customer.findOne({
        where: {
            Email: email,
            Password: Password
        }
    }).then(function (content) {

        if (content) {
            var values = {

                IsSuccess: true,
                data: content,
                Message: "Login Successfull!"
            }
            return res.send(values);
        } else {

            res.json({ success: false, message: "Email address or Password is Incorrect " });
        }

    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Services: null,
            Practitioners: null,
            Message: "Something went wrong, please try again later!"
        }
        return res.send(values);
    });;

}
function GetAllServicesAndPractionersInPractice(req, res) {

    var services = [];
    var practitioners = [];
    var practitionersService = [];
    Service.findAll(
        {
            include: [
                { model: ServiceType }
            ]
        }
    ).then(function (content) {
        services = content;


        Practitioner.findAll({
            where: {
                PracticeId: req.body.practiceId,
            }
        }).then(function (content) {
            practitioners = content;
            PractitionerService.findAll({
                include: [
                    { model: Practitioner },
                    { model: Service }
                ],

            }).then(function (content) {
              
                
                //return res.send(values);

                practitionersService = content;

                var values = {

                    IsSuccess: true,
                    Services: services,
                    Practitioners: practitioners,
                    PractitionersService: practitionersService
                }

                return res.send(values);
            }).catch(function (error) {

                console.log('Unable to connect to the database: ', error);

                var values = {

                    IsSuccess: false,
                    Practitioners: null,
                    Service: null,
                    Message: "Something went wrong, please try again later!"
                }

                return res.send(values);
            });



        }).catch(function (error) {

            console.log('Unable to connect to the database: ', error);

            var values = {

                IsSuccess: false,
                Services: null,
                Practitioners: null,
                Message: "Something went wrong, please try again later!"
            }

            return res.send(values);
        });


    }).catch(function (error) {

        console.log('Unable to connect to the database: ', error);

        var values = {

            IsSuccess: false,
            Services: null,
            Practitioners: null,
            Message: "Something went wrong, please try again later!"
        }
        return res.send(values);
    });

}


function GetAllAppointmentsOfPractitioner(req, res) {
    var appointments = [];
    var practitioner = {};
    var service = {};
    var jsonTime = '[{ "startTime": "2017-04-22T09:00:00+02:00", "endTime": "2017-04-22T12:00:00+02:00","slot":"15"}, { "startTime": "2017-05-11T18:00:00+02:00", "endTime": "2017-05-11T20:00:00+02:00","slot":"20"}, { "startTime": "2017-04-27T18:00:00+02:00", "endTime": "2017-04-27T20:00:00+02:00","slot":"20"}]';

    var values = {

        IsSuccess: true,
        appointments: jsonTime,

        Message: "Success!"
    }
    return res.send(values);
}

//export all the functions
module.exports = { GetAllServicesAndPractionersInPractice, GetPractitionersOfService, getPractionerAllApointment, GetAllAppointmentsOfPractitioner, GetAllPractitioners, GetPractitionersByServiceID, insertBookingInformation, login };