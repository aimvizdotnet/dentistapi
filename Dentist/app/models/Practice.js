﻿const Sequelize = require('sequelize');

var Practice = sequelize.define('Practice', {
    ID_Praxis: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    Name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Phone: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Fax: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    addressStreet: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressStreetNumber: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressStreetMore: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressCity: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressPostalCode: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    professionalCategory: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    networkedPracticeName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    networkedPractice: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    slogan: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    picture: {
        type: Sequelize.STRING.BINARY,
        allowNull: true,
    },
    Notes: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    TaxPunktWert: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: false,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });


module.exports = Practice;
