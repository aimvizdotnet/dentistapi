const Sequelize = require('sequelize');

var User = sequelize.define('User', {
    User_Code: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    First_Name: {
        type: Sequelize.STRING
    },
    Last_Name: {
        type: Sequelize.STRING
    },
    Email_Address: {
        type: Sequelize.STRING
    },
    Password: {
        type: Sequelize.STRING
    },
    Photo_URL: {
        type: Sequelize.STRING
    },
    Plan_Code: {
        type: Sequelize.STRING
    }
}, {
    freezeTableName: true, 
    timestamps: false,
    });

//function GetAllUsers(req, res) {
//    User.sync({}).then(function () {
//        return User.findAll()
//            .then(function (content) {
//                res.send(content);
//            });
//    }).catch(function (error) {
//        console.log('Unable to connect to the database: ', error);
//    });
//}

module.exports = User;