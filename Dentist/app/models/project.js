const Sequelize = require('sequelize');

var Project = sequelize.define('Project', {
    Project_Code: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    Project_Name: {
        type: Sequelize.STRING
    },
    Project_Description: {
        type: Sequelize.STRING
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false,
});

module.exports = Project;