﻿const Sequelize = require('sequelize');
//let Practice = require('../models/Practice');
//let Practitioner = require('../models/Practitioner');


var Appointment = sequelize.define('Appointment', {
    ID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    StartTime: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    EndTime: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Title: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    PatientName: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Description: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Color: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    LockingVersion: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Type: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    PersonCalendar: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    ResourceCalendar: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });

module.exports = Appointment;