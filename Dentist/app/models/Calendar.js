﻿const Sequelize = require('sequelize');

var Calender = sequelize.define('Calender', {
    ID_Calendar: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    Name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    DefaultView_Calendar: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    freeSlotsBookable: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    DefaultRelatedCalendar: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Type: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });


module.exports = Calender;
