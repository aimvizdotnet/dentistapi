﻿const Sequelize = require('sequelize');

var Customer = sequelize.define('Customer', {
    ID_Endkunde: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    Name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Phone: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Email: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Password: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressStreet: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressStreetNumber: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressCity: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressPostalCode: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    dateOfBirth: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });

module.exports = Customer;