﻿const Sequelize = require('sequelize');

var PractitionerService = sequelize.define('Practitioner_Service', {
    Practitoner_Service_ID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    ID_Service: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    ID_DLE_rbringer: {
        type: Sequelize.INTEGER,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });



module.exports = PractitionerService;