﻿const Sequelize = require('sequelize');
let Customer = require('../models/Customer');
let Services = require('../models/Services');

var Online_Booking = sequelize.define('Online_Booking', {
    ID_Online_Buchung: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    ID_Customer: {                  //foreign Key from CUSTOMER
        type: Sequelize.INTEGER,

        references: {
            // This is a reference to another model
            model: Customer,

            // This is the column name of the referenced model
            key: Customer.ID_Endkunde,

            // This declares when to check the foreign key constraint. PostgreSQL only.
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    ID_Service: {               //foreign Key from Services
        type: Sequelize.INTEGER,

        references: {
            // This is a reference to another model
            model: Services,

            // This is the column name of the referenced model
            key: Services.ID_DLE_S_P,

            // This declares when to check the foreign key constraint. PostgreSQL only.
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    TimeStamp: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    TermsAccepted: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    },
    dataPrivacyAccepted: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    comment: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    confirmed: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    appointmentStart: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    appointmentEnd: {
        type: Sequelize.DATE,
        allowNull: true,
    },
}, {
        freezeTableName: true,
        timestamps: false,
    });

module.exports = Online_Booking;