﻿const Sequelize = require('sequelize');

var Practitioner = sequelize.define('Practitioner', {
    ID_DLE_rbringer: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    Title: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    phone: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    addressStreet: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressStreetNumber: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressCity: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addressPostalCode: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    profession: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    professionalCategory: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    picture: {
        type: Sequelize.STRING.BINARY,
        allowNull: true,
    },
    description: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    Notes: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    taxpunktwert: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    PracticeId: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    ServiceId: {
        type: Sequelize.INTEGER,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });



module.exports = Practitioner;