﻿const Sequelize = require('sequelize');
//let Practitioner = require('../models/Practitioner');
//let Service = require('../models/Services');

var ServiceType = sequelize.define('Service_Type', {
    ID_Service_Type: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    Name: {                 
        type: Sequelize.STRING,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,

    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });


module.exports = ServiceType;