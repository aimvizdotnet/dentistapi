﻿const Sequelize = require('sequelize');
let Customer = require('../models/Customer');

var Login = sequelize.define('Login', {
    ID_Login: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    ID_Login_Endkunde: {    //foreign Key from CUSTOMER
        type: Sequelize.INTEGER,

        references: {
            // This is a reference to another model
            model: Customer,

            // This is the column name of the referenced model
            key: Customer.ID_Endkunde,

            // This declares when to check the foreign key constraint. PostgreSQL only.
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    Email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Password: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: false,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });

module.exports = Login;