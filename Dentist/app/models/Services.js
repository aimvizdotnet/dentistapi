﻿const Sequelize = require('sequelize');
let ServiceType = require('../models/ServiceType');

var Services = sequelize.define('Service', {
    ID_Service: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    ID_DLE_S_P: {

        type: Sequelize.INTEGER,
        allowNull: true,
    },
    fixedPrice: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    minPrice: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    maxPrice: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    duration: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    currency: {
        type: Sequelize.STRING,
        allowNull: true,

    },
    showPrice: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    showDuration: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    Mutuser: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    Muted: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    ServiceTypeId: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    Type: {
        type: Sequelize.STRING,
        allowNull: true,
    }
}, {
        freezeTableName: true,
        timestamps: false,
    });


module.exports = Services;