//var user = require('../controllers/user');
var booking = require('../controllers/booking');
var home = require('../controllers/home');
var jwt = require('jsonwebtoken');

module.exports = function (app, db) {



    // unauthenticated methods.
    app.route('/home/getallservicesandpractitionersinpractice').post(home.GetAllServicesAndPractionersInPractice);
    app.route('/home/getpractitionersofservice').post(home.GetPractitionersOfService);
    app.route('/home/getallappointmentsofpractitioner').post(home.GetAllAppointmentsOfPractitioner);
    app.route('/home/getallpractitioners').post(home.GetAllPractitioners);
    app.route('/home/getpractitionersbyserviceid').post(home.GetPractitionersByServiceID);
    
    app.route('/home/insertBookingInformation').post(home.insertBookingInformation);
    app.route('/home/getPractionerAllApointment').post(home.getPractionerAllApointment);
    app.route('/home/login').post(home.login);
    // route middleware to verify a token
    app.use(function (req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });

    // secure methods.
    //app.route('/Users').post(user.GetAllUsers);

   // app.route('/Projects').post(project.GetAllProjects);

  //  app.route('/Projects/:id').post(project.GetProjectByID);
};