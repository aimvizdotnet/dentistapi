﻿
angular.module("dentist", ['ngRoute', 'ngSanitize', 'hSweetAlert', 'cgNotify', 'angularFileUpload', 'ngTagsInput', 'bootstrapLightbox', 'ngCookies', 'localytics.directives', 'ng-mfb'])
.run(function ($rootScope, $http, $location, $cookies) {
    $rootScope.isLoading = true;

    $rootScope.ServerUrl = "http://localhost:5858/";
    function GetCookie(name) {
        var arg = name + "=";
        var alen = arg.length;
        var clen = document.cookie.length;
        var i = 0;
        while (i < clen) {
            var j = i + alen;
            if (document.cookie.substring(i, j) == arg) {
                return getCookieVal(j);
            }
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) break;
        }
        return null;
    }

    var delete_cookie = function (name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    //hide loading gif
    $rootScope.$on('$routeChangeSuccess', function () {
        console.log('$routeChangeSuccess');
        $rootScope.isLoading = false;
    });

    var accessToken = localStorage.getItem('accessToken');

    var documentAppCookie = $cookies.get(".AspNet.ApplicationCookie");

    //if (!accessToken || documentAppCookie == null) {
    //    //Remove Token
    //    localStorage.removeItem('userName');
    //    localStorage.removeItem('accessToken');
    //    localStorage.removeItem('refreshToken');

    //    //Remove Cookie
    //    delete_cookie(".AspNet.ApplicationCookie");
    //}

    // register listener to watch route changes
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        //checking for page authorization
        //var pageName = next.templateUrl;
        //$http.get("/api/RoleAccess/HasAccess?pageName="+pageName)
        //.then(function(response){
        //    var res = response;
        //    if (res.data == 404) {
        //        window.location = "/unauthorize";
        //    }
        //    else if(res.data == 401) {
        //        window.location = "/unauthorize";
        //    }
        //})
        //.catch(function(){
        //});

        var user = localStorage.getItem('accessToken');

        if (!user || user == undefined || user == null) {
            // no logged user, we should be going to #login
            if (next.templateUrl == "/templates/listcategories" || next.templateUrl == "/templates/mypostcards" || next.templateUrl == "/templates/uploadpostcard" || next.templateUrl == "/templates/mysettings") {
                // already going to #login, no redirect needed
                window.location = "/login";
            } else {
                // not going to #login, we should redirect now
            }
        }
        else {
            if (next.templateUrl == "/templates/login" || next.templateUrl == "/templates/register") {
                // already going to #login, no redirect needed
                localStorage.removeItem('userName');
                localStorage.removeItem('accessToken');
                localStorage.removeItem('refreshToken');

                //window.location = "/mypostcards";
            }
        }
    });


    var accesstoken = localStorage.getItem('accessToken');

    var authHeaders = {};
    if (accesstoken) {
        authHeaders.Authorization = 'Bearer ' + accesstoken;
    }

    var config = {
        headers: {
            'Authorization': 'Bearer ' + accesstoken,
        }
    };

    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accesstoken;
})
.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    // Wire up the traffic cop interceptors. This method will be invoked with
    // full dependency-injection functionality.
    // --
    // NOTE: This approach has been available since AngularJS 1.1.4.
    $httpProvider.interceptors.push(interceptHttp);
    // We're going to TRY to track the outgoing and incoming HTTP requests.
    // I stress "TRY" because in a perfect world, this would be very easy
    // with the promise-based interceptor chain; but, the world of
    // interceptors and data transformations is a cruel she-beast. Any
    // interceptor may completely change the outgoing config or the incoming
    // response. As such, there's a limit to the accuracy we can provide.
    // That said, it is very unlikely that this will break; but, even so, I
    // have some work-arounds for unfortunate edge-cases.
    function interceptHttp($q) {
        // Return the interceptor methods. They are all optional and get
        // added to the underlying promise chain.
        return ({
            request: request,
            requestError: requestError,
            response: response,
            responseError: responseError
        });
        // ---
        // PUBLIC METHODS.
        // ---
        // Intercept the request configuration.
        function request(config) {
            // NOTE: We know that this config object will contain a method as
            // this is the definition of the interceptor - it must accept a
            // config object and return a config object.
            //trafficCop.startRequest(config.method);
            // Pass-through original config object.
            return (config);
        }
        // Intercept the failed request.
        function requestError(rejection) {
            // At this point, we don't why the outgoing request was rejected.
            // And, we may not have access to the config - the rejection may
            // be an error object. As such, we'll just track this request as
            // a "GET".
            // --
            // NOTE: We can't ignore this one since our responseError() would
            // pick it up and we need to be able to even-out our counts.
            //trafficCop.startRequest("get");
            // Pass-through the rejection.
            return ($q.reject(rejection));
        }
        // Intercept the successful response.
        function response(response) {
            //trafficCop.endRequest(extractMethod(response));
            // Pass-through the resolution.
            return (response);
        }
        // Intercept the failed response.
        function responseError(response) {
            //trafficCop.endRequest(extractMethod(response));
            // Pass-through the rejection.

            if (response.status == 401) {
                window.location = "/unauthorize";
            }
            else if (response.status == 500) {
                window.location = "/error";
            }
            return ($q.reject(response));
        }
        // ---
        // PRIVATE METHODS.
        // ---
        // I attempt to extract the HTTP method from the given response. If
        // another interceptor has altered the response (albeit a very
        // unlikely occurrence), then we may not be able to access the config
        // object or the the underlying method. If this fails, we return GET.
        function extractMethod(response) {
            try {
                return (response.config.method);
            } catch (error) {
                return ("get");
            }
        }
    }

    //================================================
    // Routes
    //================================================
    $routeProvider.when('app/home', {
        templateUrl: '/Templates/index.html',
    	controller: 'homeController',
        controllerAs: 'homeController',
    	caseInsensitiveMatch: true
    });

    $routeProvider
      .otherwise({
          redirectTo: '/error'
      });

    $locationProvider.html5Mode(true);
}])