﻿(function (angular) {
    'use strict';

    angular
     .module('dentist')
     .factory('bookingService', ['$http', bookingServiceFactory]);

    function bookingServiceFactory($http) {

        var bookingService = {
            getAllBookings: getAllBookings,
            addBooking: addBooking,
        };

        return bookingService;

        //////////////////////////////////////////////////////

        function getAllBookings() {
            return $http.get("/api/Service/getAllBookings")
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };


        function addBooking(data) {
            return $http.post("/api/Service/addBooking",data)
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };

    }
})(window.angular);