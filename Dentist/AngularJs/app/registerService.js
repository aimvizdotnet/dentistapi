﻿(function (angular) {
    'use strict';

    angular
       .module('dentist')
       .factory('registerService', ['$http', registerServiceFactory]);

    function registerServiceFactory($http) {
        var registerService = {
            registerUser: registerUser
        };

        return registerService;

        // Method implementations

        function registerUser(user) {

            return $http.post("/api/Account/Register", user)
                    .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }

        }
    }

})(window.angular);