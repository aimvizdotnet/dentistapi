﻿(function (angular) {
    'use strict';
    angular.module("dentist").controller('homeController', homeController);

    function homeController($location, dentistService, directoryService, settingsService) {

        var ctrl = this;

        // bindable properties
        ctrl.searchTerm = '';
        ctrl.ShowLoader = false;
        ctrl.Services = {};

        // bindable methods
        ctrl.getServices = getServices;

        init();

        function init() {

        }

        function getServices() {

            dentistService.getAllServices().then(function (res) {
                var result = res.data;
                if (result.IsSuccess) {

                    ctrl.Services = res.data.Data;

                }
                else {


                }
            }, function (error) {
                $scope.divLoginError = true;
                $ionicLoading.hide();
            });

        }

        ctrl.getServices();

    }

    homeController.$inject = ['$location', 'homeService', 'directoryService', 'settingsService'];

})(window.angular);