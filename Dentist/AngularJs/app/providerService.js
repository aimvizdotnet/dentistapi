﻿(function (angular) {
    'use strict';

    angular
     .module('dentist')
     .factory('providerService', ['$http', providerServiceFactory]);

    function providerServiceFactory($http) {

        var providerService = {
            getAllProviders: getAllProviders,
            getAllProvidersByServiceId:getAllProvidersByServiceId
        };

        return providerService;

        //////////////////////////////////////////////////////

        function getAllProviders() {
            return $http.get("/api/Service/GetAllProviders")
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };

        function getAllProvidersByServiceId(serviceId) {
            return $http.post("/api/Service/getAllProvidersByServiceId",serviceId)
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };

    }
})(window.angular);