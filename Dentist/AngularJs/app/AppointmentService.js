﻿(function (angular) {
    'use strict';

    angular
     .module('dentist')
     .factory('appointmentService', ['$http', appointmentServiceFactory]);

    function appointmentServiceFactory($http) {

        var appointmentService = {
            getAllAppointments: getAllAppointments,
            getAllAppointmentsByProvidersId: getAllAppointmentsByProvidersId,
        };

        return appointmentService;

        //////////////////////////////////////////////////////

        function getAllAppointments() {
            return $http.get("/api/Service/GetAllAppointments")
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };


        function getAllAppointmentsByProvidersId(providerId) {
            return $http.post("/api/Service/getAllAppointmentsByProvidersId", providerId)
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };

    }
})(window.angular);