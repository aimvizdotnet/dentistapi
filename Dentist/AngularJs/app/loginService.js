﻿(function (angular) {
    'use strict';

    angular
     .module('dentist')
     .factory('loginService', ['$http', loginServiceFactory]);

    function loginServiceFactory($http) {

        var loginService = {
            loginUser: loginUser,
            logoutUser: logoutUser
        };

        return loginService;

        //////////////////////////////////////////////////////

        function loginUser(user) {
            return $http.post("/api/Account/Login",
                $.param({username: user.email, password: user.password })            )
            .then(getAllComplete)
			.catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        }

        function logoutUser() {
            return $http.post("/api/Account/LogOut")
            .then(getAllComplete)
			.catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        }

    }
})(window.angular);