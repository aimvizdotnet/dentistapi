﻿(function (angular) {
    'use strict';

    angular
     .module('dentist')
     .factory('dentistService', ['$http', dentistServiceFactory]);

    function dentistServiceFactory($http) {

        var dentistService = {
            getAllServices: getAllServices,
        };

        return dentistService;

        //////////////////////////////////////////////////////

        function getAllServices() {
            return $http.get("/home/GetAllServices")
                .then(getAllComplete)
				    .catch(getAllError);

            function getAllComplete(response) {
                return response;
            }

            function getAllError(error) {
                // any error logging goes here
                console.log(error);

                throw error;
            }
        };

    }
})(window.angular);